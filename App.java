public class App {
    public static int GaussianSum(int n) {
        int sum = 0;
        for(int i = 0; i <= n; i++) {
            sum += i;
        }
        return sum;
    }

    public static void main(String[] args) {
        long startNS, endNS;
        int n;

        if(args.length < 1) {
            System.out.println("No 'n' set by CLI! Using 32!");
            n = 32;
        } else {
            try {
                n = Integer.parseInt(args[0]);
            } catch(NumberFormatException e) {
                System.out.println("Invalid 'n' set! Using 32!");
                n = 32;
            }
        }

        startNS = System.nanoTime();
        int gaussianSum = GaussianSum(n);
        endNS = System.nanoTime();
        
        System.out.println("Gaussian Sum of " + n + " is: " + gaussianSum);
        System.out.println("Took: " + (endNS - startNS) + "ns");
    }
}
# Gaussian Sum Algorithm Analysis
The gaussian sum algorithm adds a series of finite numbers together.

## Mathematical perspective
Assuming we have a set of values, the gaussian sum algorithm performs an addition on all entries.
```
S       := {1, 2, .., n} <- finite numbers
sum(n)  := 1 + 2 + .. + n
         = gaussian sum
```
Example:
```
S   := {1, 2, 3, 4}
sum := 1 + 2 + 3 + 4
     = 10
```

## Programmatic perspective
When programming a gaussian sum algorithm, we need a parameter that defines how many numbers we want to add.   
We call it `n`.

Additionally, we need a variable to store the result which will be returned at the end of our function.   
We call it `sum`.

Now for the main part:  
We create a numerical for-loop that loops until `n` is reached.  
Each iteration is stored in `i`.  
With each iteration we add the current iteration, `i`, to our end result, `sum`.

### In java this might look like:
```java
int gaussianSum(int n) {
    int sum;
    for(int i = 0; i <= n; i++) {
        sum += i;
    }
    return sum;
}
```
> See `App.java` for more :)

___

# Theoretical analysis
The higher we set the upper limit (`n`), the more loops are required.
Thus, the execution time will increase.

### Proof
If we compare executing the algorithm with `n = 30` and `n = 300` we notice, that the last one runs 10 times slower than the first one.  
```
30 * 10 = 300
```

### Result
The execution time of this algorithm grows linear with an increasing upper limit (`n`).

# Emperical analysis
Before and after calling the algorithm we either start and stop a timer or store the current time.
Independent of the method used, the method must yield an accurate result.  
Thus, we need either milliseconds or nanoseconds to measure.

### In Java
In Java we can get a `long` of the current system time in nanoseconds via `System::nanoseconds()`.
We store this value before and after calling the algorithm:
```java
long before = System.nanotime();
int sum = gaussianSum(32);
long after = System.nanotime();
```
> See `App.java` for more :)

Afterwards, we subtract the value measured **after** the algorithm, from the value measured **before** the algorithm.

Ideally, the upper limit is read as an parameter to the application (like `App.java` is!) or many calls and measures are done by the application.

### Results
| `n`       | Execution time    |
| --------- | ----------------- |
| 1         | 1400ns            |
| 2         | 1400ns            |
| 4         | 1400ns            |
| 8         | 1400ns            |
| 16        | 1400ns            |
| 32        | 1400ns            |
| 64        | 1800ns            |
| 1024      | 11600ns           |
| 10240     | 95400ns           |
| 102400    | 894800ns          |
| 1024000   | 1890800ns         |
| 10240000  | 6744400ns         |
> Results can be reproduced by running `App.java` with `n` (e.g. `java App.java 1024`)
> For better results run multiple times and use the average/mean result

First of all a side note:  
Why is the execution time from numbers 1 to 32 the same?  
It's hard to find out the exact cause, but it most likely has to do with Java.
Java optimizes our code heavily while compiling and sometimes even while running.
It's probably good to optimize for small numbers but harder for bigger numbers.
It may also has to do with memory allocation and usage as bigger numbers require more space.  
**In other words:** If we would use a native language, such as C or C++ (without optimizations), we would get a near linear result.

From our result we can confirm that the execution time does roughly linear grow.

### In Rust
```rust
fn gaussian_sum(n: i32) -> i32 {
    let mut result: i32 = 0;
    for i in 0..n {
        result += i;
    }
    return result;
}
```
> See `App.rs` for more :)

### Results
| `n`       | Execution time                    |
| --------- | --------------------------------- |
| 1         | 700ns / 0mc / 0ms                 |
| 2         | 900ns / 1mc / 0ms                 |
| 4         | 600ns / 0mc / 0ms                 |
| 8         | 800ns / 1mc / 0ms                 |
| 16        | 1200ns / 1mc / 0ms                |
| 32        | 1700ns / 1mc / 0ms                |
| 64        | 2700ns / 2mc / 0ms                |
| 1024      | 68100ns / 68mc / 0ms              |
| 10240     | 315700ns / 315mc / 0ms            |
| 102400    | 3473200ns / 3473mc / 3ms          |
| 1024000   | 34411100ns / 34411mc / 34ms       |
| 10240000  | 379709800ns / 379710mc / 379ms    |

> Results can be reproduced with compiling `App.rs` (e.g. `rustc App.rs`) and running the application with `n` (`./App 1024`)
> For better results run multiple times and use the average/mean result

Lower, but similar results!
Additionally, there is no magic point from which the execution time suddenly increases.

# The problem
The problem with this algorithm is simple: `N`!
The greater `N` gets, the more loops have to be done and thus, the execution time increases almost linearly.

How to solve that?
With **maths**!

We can write a formula like:
```
2 * sum(N) = N * (N + 1)
```
Or even simpler:
```
sum(N) = N * (N + 1) / 2
```
Example / Proof:
```
sum(4)  = 4 * (4 + 1) / 2
        = 4 * (5) / 2
        = 20 / 2
        = 10
```

Implementing this in any language just requires three mathematical operations and **will be the fastest way possible**.